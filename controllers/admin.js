const Product = require('../models/product');

exports.getAddProduct = (req, res, next) => {
  res.render('admin/edit-product', {
    pageTitle: 'Add Product',
    path: '/admin/add-product',
    editing: false
  });
};

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  const product = new Product({
    title: title,
    price: price,
    description: description,
    imageUrl: imageUrl,
    userId: req.user  // mongose due to we have a "ref" for userId in the schema models/user.js then just will save the id not all the object in the DB
  });
  product
    .save()
    .then(result => {
      // console.log(result);
      console.log('Created Product');
      res.redirect('/admin/products');
    })
    .catch(err => {
      console.log(err);
    });
};

exports.getEditProduct = (req, res, next) => {
  const editMode = req.query.edit;
  if (!editMode) {
    return res.redirect('/');
  }
  const prodId = req.params.productId;
   // findById() method is created/native by mongoose
  Product.findById(prodId)
    // Product.findById(prodId)
    .then(product => {
      if (!product) {
        return res.redirect('/');
      }
      res.render('admin/edit-product', {
        pageTitle: 'Edit Product',
        path: '/admin/edit-product',
        editing: editMode,
        product: product
      });
    })
    .catch(err => console.log(err));
};

exports.postEditProduct = (req, res, next) => {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const updatedImageUrl = req.body.imageUrl;
  const updatedDesc = req.body.description;
  // const product = new Product({ // passing parameters to the class constructor using the 'mongoose way' to do it
  //   title: title,
  //   price: price,
  //   description: description,
  //   imageUrl: imageUrl
  // });

  Product.findById(prodId).then((prod) => {

  })
  .catch((err) => {

  }),
  //findById() method is created/native by mongoose
  Product.findById(prodId)
    .then(product => {
      /** The great things is that Mongoose if you are "saving" an data that already exist, its smart to understand that the operacion to do is UPDATE that data */
      product.title = updatedTitle;
      product.price = updatedPrice;
      product.description = updatedDesc;
      product.imageUrl = updatedImageUrl;
      return product.save(); // save() method is created/native by mongoose ( we are not creating that)
    })
    .then(result => { // save()   returns  a promisse, due to that the "then" stament here
      console.log('UPDATED PRODUCT!');
      res.redirect('/admin/products');
    })
    .catch(err => console.log(err));
};

exports.getProducts = (req, res, next) => {
   // find() method is created/native by mongoose
  Product.find()
    // .select('title  price description imageUrl -_id')// bring me the data except _id(-)
    // .populate('userId', 'name') // create a object [userId] With name into it
    .then(products => {
      res.render('admin/products', {
        prods: products,
        pageTitle: 'Admin Products',
        path: '/admin/products'
      });
    })
    .catch(err => console.log(err));
};

exports.postDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  //findByIdAndDelete() method is created/native by mongoose
  Product.findByIdAndDelete(prodId)
    .then(() => {
      console.log('DESTROYED PRODUCT');
      res.redirect('/admin/products');
    })
    .catch(err => console.log(err));
};
